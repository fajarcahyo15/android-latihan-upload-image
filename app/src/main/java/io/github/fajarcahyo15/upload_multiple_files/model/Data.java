package io.github.fajarcahyo15.upload_multiple_files.model;

public class Data {

    public static final String TABLE_NAME = "data";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_FOTO = "foto";

    private int id;
    private String foto;

    public static final String CREATE_TABLE = "CREATE TABLE " +
            TABLE_NAME +
            "( "+
            COLUMN_ID +" INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_FOTO +" TEXT " +
            ")";
    public Data() {}

    public Data(String foto) {
        this.foto = foto;
    }

    public Data(int id, String foto) {
        this.id = id;
        this.foto = foto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
