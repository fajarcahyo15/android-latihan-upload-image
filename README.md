<h2>Latihan Upload Gambar</h2>

<ul>
    <li>Bahasa Server: <a href="http://php.net/">PHP</a></li>
    <li>Target SDK: 26</li>
    <li>Local DB: <a href="https://www.sqlite.org/index.html">SQLite</a></li>
    <li> Library: 
        <ul>
            <li><a href="https://github.com/googlesamples/easypermissions">EasyPermission</a></li>
            <li><a href="https://github.com/jkwiecien/EasyImage">EasyImage</a></li>
            <li><a href="http://square.github.io/retrofit/">Retrofit 2</a></li>
            <li><a href="http://jakewharton.github.io/butterknife/">ButterKnife</a></li>
            <li><a href="https://github.com/bumptech/glide">Glide</a></li>
        </ul>
    </li>
</ul>