package io.github.fajarcahyo15.upload_multiple_files.presenter;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.github.fajarcahyo15.upload_multiple_files.adapter.GambarAdapter;
import io.github.fajarcahyo15.upload_multiple_files.helper.retrofit.BodyRequest;
import io.github.fajarcahyo15.upload_multiple_files.helper.retrofit.RestApiClient;
import io.github.fajarcahyo15.upload_multiple_files.helper.retrofit.RestApiService;
import io.github.fajarcahyo15.upload_multiple_files.model.Data;
import io.github.fajarcahyo15.upload_multiple_files.model.Laporan;
import io.github.fajarcahyo15.upload_multiple_files.model.Pengajuan;
import io.github.fajarcahyo15.upload_multiple_files.model.database.DataHelper;
import io.github.fajarcahyo15.upload_multiple_files.view.MainView;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.AsyncSubject;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPresenter implements Presenter<MainView> {

    private MainView mainView;
    private DataHelper dataHelper;



    @Override
    public void onAttach(final MainView view) {
        mainView = view;
    }

    @Override
    public void onDetach() {
        mainView = null;
    }

    private void showPesanKesalahan(Context context) {
        Toast.makeText(context, "Terjadi Kesalahan!", Toast.LENGTH_SHORT).show();
    }

    public void setActionBar() {
        mainView.onSetActionBar();
    }

    // mendapatkan seluruh data pada sqlite yang diberikan dalam bentuk adapter
    public void showRecyclerView(Context context) {
        dataHelper = new DataHelper(context);
        GambarAdapter gambarAdapter = new GambarAdapter(context, dataHelper.getAllData());
        mainView.setRecyclerView(gambarAdapter);
    }

    // mencatat nama file foto kedalam sqlite
    public void tambahFoto(Data data, Context context) {
        dataHelper = new DataHelper(context);
        dataHelper.insertData(data);
    }

    // method simpan foto ke internal storage
    public Boolean simpanFoto(Context context, List<File> imageFiles) {
        dataHelper = new DataHelper(context);
        Boolean sukses = false;

        // fetch data
        for (int i = 0; i < imageFiles.size(); i++) {

            // mendapatkan id terakhir
            int id = 0;
            if (!dataHelper.getLastId().equals(null)) {
                id = dataHelper.getLastId().getId();
                id += 1;
            }

            // membuat file dan nama gambar baru
            Bitmap bitmap = BitmapFactory.decodeFile(imageFiles.get(i).getPath());
            String fileName = "pengajuan"+String.valueOf(id)+".jpeg";
            File file = new File(context.getFilesDir(), fileName);

            Log.d("FileDir", "simpanFoto: "+context.getFilesDir());

            // jika file sudah ada maka delete
            if (file.exists()) file.delete();

            // simpan file gambar ke dalam direktori internal
            try {
                FileOutputStream out = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 40, out);
                out.flush();
                out.close();

                Data data = new Data();
                data.setFoto(fileName);
                tambahFoto(data, context);
                sukses = true;
            } catch (Exception e) {
                e.printStackTrace();
                sukses = false;
            }
        }

        return sukses;
    }

    // dummy data pengajuan
    private Pengajuan dataPengajuan() {

        Pengajuan pengajuan = new Pengajuan();
        pengajuan.setId(1);
        pengajuan.setId_pengajuan("PGSKC-PRY001-LOG001-001");
        pengajuan.setId_sub_kas_kecil("LOG-001");
        pengajuan.setKode_proyek("PRY001");
        pengajuan.setTanggal("2018-01-01");
        pengajuan.setTotal(Double.valueOf(15500));
        pengajuan.setDana_disetujui(Double.valueOf(15500));
        pengajuan.setStatus("DISETUJUI");

        return pengajuan;

    }

    // dummy data laporan
    private List<Laporan> dataLaporan() {
        List<Laporan> listLaporan = new ArrayList<>();

        for (int i=1;i<=3;i++) {
            Laporan laporan = new Laporan();
            laporan.setId(i);
            laporan.setId_pengajuan("PGSKC-PRY001-LOG001-001");
            laporan.setNama("Barang-"+i);
            laporan.setJenis("TEKNIS");
            laporan.setSatuan("KARUNG");
            laporan.setQty(i);
            laporan.setHarga(Double.valueOf(399999));
            laporan.setSubtotal(laporan.getQty() * laporan.getHarga());
            laporan.setStatus("TUNAI");
            listLaporan.add(laporan);
        }

        return listLaporan;
    }

    // mendapatkan list file multipart
    private List<MultipartBody.Part> getPartFoto(Context context) {
        dataHelper = new DataHelper(context);
        BodyRequest br = new BodyRequest();
        List<MultipartBody.Part> foto = new ArrayList<>();

        List<Data> data = dataHelper.getAllData();
        for (int i=0;i<data.size();i++) {
            foto.add(br.prepareFilePart(context, String.valueOf(i),data.get(i).getFoto()));
        }

        return foto;
    }

    // kirim data ke server
    public void kirimLaporan(Context context) throws JSONException {

        mainView.dislpayLoading();

        RestApiService apiService = RestApiClient.getClient().create(RestApiService.class);
        Gson gson = new Gson();
        BodyRequest br = new BodyRequest();

        Call<ResponseBody> result = apiService.uploadLaporan(
                br.createBodyFromString("mobile"),
                br.createBodyFromString("asdwasdwasd"),
                br.createBodyFromString(gson.toJson(dataPengajuan())),
                br.createBodyFromString(gson.toJson(dataLaporan())),
                getPartFoto(context)
        );

        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    Log.d("response", response.body().string());
                    mainView.destroyLoading();
                } catch (IOException e) {
                    e.printStackTrace();
                    mainView.destroyLoading();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                mainView.destroyLoading();
            }
        });

    }
}
