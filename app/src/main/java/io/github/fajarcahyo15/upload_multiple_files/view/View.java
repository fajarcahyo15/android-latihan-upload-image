package io.github.fajarcahyo15.upload_multiple_files.view;



public interface View {
    void onAttachView();

    void onDetachView();
}
