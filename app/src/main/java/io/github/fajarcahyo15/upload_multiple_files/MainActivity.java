package io.github.fajarcahyo15.upload_multiple_files;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONException;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.fajarcahyo15.upload_multiple_files.adapter.GambarAdapter;
import io.github.fajarcahyo15.upload_multiple_files.model.Data;
import io.github.fajarcahyo15.upload_multiple_files.model.database.DataHelper;
import io.github.fajarcahyo15.upload_multiple_files.presenter.MainPresenter;
import io.github.fajarcahyo15.upload_multiple_files.view.MainView;
import pl.aprilapps.easyphotopicker.EasyImage;
import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends AppCompatActivity implements MainView {

    MainPresenter mainPresenter;

    AlertDialog loadingDialog;

    @BindView(R.id.rv_gambar)
    RecyclerView rv_gambar;

    @BindView(R.id.btn_upload)
    Button btn_upload;


    private static final int REQUEST_CODE_CAMERA = 001;
    private static final int REQUEST_CODE_GALLERY = 002;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // inisialisasi presenter
        mainPresenter = new MainPresenter();
        onAttachView();
        ButterKnife.bind(this);

        mainPresenter.setActionBar();
        mainPresenter.showRecyclerView(this);

        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    mainPresenter.kirimLaporan(MainActivity.this);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        onDetachView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.mn_tambah:
                dialogRequestFoto();
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        handleRequestfoto(requestCode,resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_CODE_CAMERA:
                EasyImage.openCamera(MainActivity.this, REQUEST_CODE_CAMERA);
                break;

            default:
                break;
        }
    }

    @Override
    public void onAttachView() {
        mainPresenter.onAttach(MainActivity.this);

        // konfigurasi easyimage agar bisa mengambil banyak foto dari gallery sekaligus (API 16++)
        EasyImage.configuration(this).setAllowMultiplePickInGallery(true);
    }

    @Override
    public void onDetachView() {
        mainPresenter.onDetach();

        // mereset konfigurasi easyimage
        EasyImage.clearConfiguration(this);
    }

    @Override
    public void onSetActionBar() {
        getSupportActionBar().setTitle("Latihan Upload Ke Server");
        getSupportActionBar().setSubtitle("Upload Banyak File");
    }

    @Override
    public void setRecyclerView(GambarAdapter adapter) {
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
        rv_gambar.setHasFixedSize(true);
        rv_gambar.setLayoutManager(layoutManager);
        rv_gambar.setAdapter(adapter);
    }

    @Override
    public void dislpayLoading() {
        AlertDialog.Builder loadingBuilder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();
        View loadingView = inflater.inflate(R.layout.loading_dialog, null);



        loadingDialog = loadingBuilder.setView(loadingView)
                .setCancelable(false)
                .create();
        loadingDialog.show();
    }

    @Override
    public void destroyLoading() {
        loadingDialog.dismiss();
    }

    // tampilan dialog pilihan untuk mengambil foto dari camera/gallery
    private void dialogRequestFoto() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        CharSequence[] item = {"Ambil Foto","Dari Galeri"};

        builder.setTitle("Tambah Foto")
                .setItems(item, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case 0:

                                String[] reqPermission = {Manifest.permission.CAMERA};
                                if(EasyPermissions.hasPermissions(MainActivity.this, reqPermission)) {
                                    EasyImage.openCamera(MainActivity.this, REQUEST_CODE_CAMERA);
                                } else {
                                    EasyPermissions.requestPermissions(
                                            MainActivity.this,
                                            "Digunakan untuk mengambil bukti faktur pembelian",
                                            REQUEST_CODE_CAMERA,
                                            reqPermission
                                    );
                                }

                                break;
                            case 1:
                                EasyImage.openGallery(MainActivity.this, REQUEST_CODE_GALLERY);
                                break;
                        }
                    }
                });
        builder.create();
        builder.show();
    }

    // menanagani request ambil foto dari camera/gallery
    private void handleRequestfoto(int requestCode, int resultCode, Intent data) {

        EasyImage.handleActivityResult(requestCode, resultCode, data, MainActivity.this, new EasyImage.Callbacks() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                e.printStackTrace();
                Toast.makeText(MainActivity.this, "Terjadi Kesalahan!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                switch (type) {
                    case REQUEST_CODE_CAMERA:
                        mainPresenter.simpanFoto(MainActivity.this, imageFiles);
                        break;
                    case REQUEST_CODE_GALLERY:
                        mainPresenter.simpanFoto(MainActivity.this, imageFiles);
                        break;
                    default:
                        break;
                }

                mainPresenter.showRecyclerView(MainActivity.this);
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(MainActivity.this);
                    if (photoFile != null) photoFile.delete();
                }
            }
        });
    }

}
