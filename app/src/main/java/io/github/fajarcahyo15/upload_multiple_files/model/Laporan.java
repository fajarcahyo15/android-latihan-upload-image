package io.github.fajarcahyo15.upload_multiple_files.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 4/26/18.
 */

public class Laporan {

    @SerializedName("id")
    private int id;

    @SerializedName("id_pengajuan")
    private String id_pengajuan;

    @SerializedName("nama")
    private String nama;

    @SerializedName("jenis")
    private String jenis;

    @SerializedName("satuan")
    private String satuan;

    @SerializedName("qty")
    private Integer qty;

    @SerializedName("harga")
    private Double harga;

    @SerializedName("subtotal")
    private Double subtotal;

    @SerializedName("status")
    private String status;

    public Laporan() {
    }

    public Laporan(int id, String id_pengajuan, String nama, String jenis, String satuan, Integer qty, Double harga, Double subtotal, String status) {
        this.id = id;
        this.id_pengajuan = id_pengajuan;
        this.nama = nama;
        this.jenis = jenis;
        this.satuan = satuan;
        this.qty = qty;
        this.harga = harga;
        this.subtotal = subtotal;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getId_pengajuan() {
        return id_pengajuan;
    }

    public void setId_pengajuan(String id_pengajuan) {
        this.id_pengajuan = id_pengajuan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Double getHarga() {
        return harga;
    }

    public void setHarga(Double harga) {
        this.harga = harga;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
