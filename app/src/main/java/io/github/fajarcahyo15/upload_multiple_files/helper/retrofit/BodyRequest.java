package io.github.fajarcahyo15.upload_multiple_files.helper.retrofit;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by root on 4/27/18.
 */

public class BodyRequest {

    @NonNull
    public RequestBody createBodyFromString(String descriptionString) {

        return RequestBody.create(MultipartBody.FORM, descriptionString);
    }

    @NonNull
    public MultipartBody.Part prepareFilePart(Context context, String partName, String namaFile) {

        File file = new File(context.getFilesDir(), namaFile);

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"),file);

        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }
}
