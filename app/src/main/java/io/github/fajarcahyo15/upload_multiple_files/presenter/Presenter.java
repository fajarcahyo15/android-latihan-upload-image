package io.github.fajarcahyo15.upload_multiple_files.presenter;


import io.github.fajarcahyo15.upload_multiple_files.view.View;

/**
 * Created by root on 4/22/18.
 */


public interface Presenter<T extends View> {
    void onAttach(T view);

    void onDetach();
}
