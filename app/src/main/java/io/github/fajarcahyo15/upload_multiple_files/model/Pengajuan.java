package io.github.fajarcahyo15.upload_multiple_files.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 4/26/18.
 */

public class Pengajuan {

    @SerializedName("id")
    private int id;

    @SerializedName("id_pengajuan")
    private String id_pengajuan;

    @SerializedName("id_sub_kas_kecil")
    private String id_sub_kas_kecil;

    @SerializedName("kode_proyek")
    private String kode_proyek;

    @SerializedName("tanggal")
    private String tanggal;

    @SerializedName("total")
    private Double total;

    @SerializedName("dana_disetujui")
    private Double dana_disetujui;

    @SerializedName("status")
    private String status;

    public Pengajuan() {
    }

    public Pengajuan(int id, String id_pengajuan, String id_sub_kas_kecil, String kode_proyek, String tanggal, Double total, Double dana_disetujui, String status) {
        this.id = id;
        this.id_pengajuan = id_pengajuan;
        this.id_sub_kas_kecil = id_sub_kas_kecil;
        this.kode_proyek = kode_proyek;
        this.tanggal = tanggal;
        this.total = total;
        this.dana_disetujui = dana_disetujui;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getId_pengajuan() {
        return id_pengajuan;
    }

    public void setId_pengajuan(String id_pengajuan) {
        this.id_pengajuan = id_pengajuan;
    }

    public String getId_sub_kas_kecil() {
        return id_sub_kas_kecil;
    }

    public void setId_sub_kas_kecil(String id_sub_kas_kecil) {
        this.id_sub_kas_kecil = id_sub_kas_kecil;
    }

    public String getKode_proyek() {
        return kode_proyek;
    }

    public void setKode_proyek(String kode_proyek) {
        this.kode_proyek = kode_proyek;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getDana_disetujui() {
        return dana_disetujui;
    }

    public void setDana_disetujui(Double dana_disetujui) {
        this.dana_disetujui = dana_disetujui;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
