package io.github.fajarcahyo15.upload_multiple_files.helper.retrofit;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by root on 4/26/18.
 */

public interface RestApiService {

//    @FormUrlEncoded
//    @POST("/")
//    Call<ResponseBody> uploadLaporan(
//            @Field("tipe") String tipe,
//            @Field("token") String token,
//            @Field("pengajuan") String pengajuan,
//            @Field("laporan") String listLaporan
//            );

    @Multipart
    @POST("index.php")
    Call<ResponseBody> uploadLaporan(
            @Part("tipe") RequestBody tipe,
            @Part("token") RequestBody token,
            @Part("pengajuan") RequestBody pengajuan,
            @Part("laporan") RequestBody listLaporan,
            @Part List<MultipartBody.Part> bukti
            );
}
