package io.github.fajarcahyo15.upload_multiple_files.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;
import java.util.List;

import io.github.fajarcahyo15.upload_multiple_files.R;
import io.github.fajarcahyo15.upload_multiple_files.helper.GlideApp;
import io.github.fajarcahyo15.upload_multiple_files.model.Data;
import io.github.fajarcahyo15.upload_multiple_files.model.database.DataHelper;

/**
 * Created by root on 4/22/18.
 */

public class GambarAdapter extends RecyclerView.Adapter<GambarAdapter.MyViewHolder> {

    private Context mContext;
    private List<Data> datas;

    public GambarAdapter(Context context, List<Data> data) {
        mContext = context;
        datas = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gambar_adapter, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GambarAdapter.MyViewHolder holder, final int position) {
        final int currentPosition = position;
        Data data = datas.get(position);
        File file = new File(mContext.getFilesDir(), data.getFoto());

        GlideApp.with(mContext)
                .asBitmap()
                .load(file.getPath())
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(holder.iv_foto);

        holder.iv_foto.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                Data data = datas.get(currentPosition);
                dialogHapusFoto(data, position);

                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView iv_foto;
        public View view;

        public MyViewHolder(View itemView) {
            super(itemView);
            iv_foto = itemView.findViewById(R.id.iv_foto);

        }
    }

    private void dialogHapusFoto(final Data data, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        builder.setTitle("Hapus Foto")
                .setMessage("Hapus Foto "+data.getFoto())
                .setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        hapusFoto(data,position);
                    }
                })
                .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .setCancelable(true);

        builder.create();
        builder.show();
    }

    // method menghapus file gambar dan menghapus data dari sqlite
    private void hapusFoto(Data data, int position) {
        DataHelper dataHelper = new DataHelper(mContext);
        File file = new File(mContext.getFilesDir(), data.getFoto());

        Log.d("FileDir", "hapusFoto: "+mContext.getFilesDir());

        try {
            file.delete();
            dataHelper.hapusData(data);
            datas.remove(position);
            notifyDataSetChanged();
            Toast.makeText(mContext, "Foto telah dihapus", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(mContext, "Terjadi Kesalahan!", Toast.LENGTH_SHORT).show();
        }
    }

}
