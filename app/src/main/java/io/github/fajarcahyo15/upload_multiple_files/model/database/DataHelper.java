package io.github.fajarcahyo15.upload_multiple_files.model.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import io.github.fajarcahyo15.upload_multiple_files.model.Data;


public class DataHelper extends SQLiteOpenHelper {

    public DataHelper(Context context) {
        super(context, Database.DATABASE_NAME, null, Database.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(Data.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Data.TABLE_NAME);

        onCreate(sqLiteDatabase);
    }

    public long insertData(Data data) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(Data.COLUMN_FOTO, data.getFoto());

        long id = db.insert(Data.TABLE_NAME,null,values);

        db.close();

        return id;
    }

    // cek data kosong
    public Boolean dataKosong() {
        Boolean kosong = false;
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT count(*) FROM "+Data.TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        int count = cursor.getInt(0);
        if (count <= 0) {
            kosong = true;
        }

        return kosong;
    }

    // mendapatkan seluruh data
    public List<Data> getAllData() {
        List<Data> datas = new ArrayList<>();

        String query = "SELECT * FROM " + Data.TABLE_NAME +
                " ORDER BY " + Data.COLUMN_ID + " DESC";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                Data data = new Data();
                data.setId(cursor.getInt(cursor.getColumnIndex(Data.COLUMN_ID)));
                data.setFoto(cursor.getString(cursor.getColumnIndex(Data.COLUMN_FOTO)));
                datas.add(data);
            } while (cursor.moveToNext());
        }

        db.close();
        return datas;
    }

    // mendapatkan data berdasarkan id
    public Data getData(long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Data.TABLE_NAME,
                new String[]{Data.COLUMN_ID,Data.COLUMN_FOTO},
                Data.COLUMN_ID + "=?",
                new String[]{String.valueOf(id)},null,null,null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        Data data = new Data(
            cursor.getInt(cursor.getColumnIndex(Data.COLUMN_ID)),
            cursor.getString(cursor.getColumnIndex(Data.COLUMN_FOTO))
        );

        cursor.close();
        db.close();

        return data;
    }

    // mendapatkan id terakhir
    public Data getLastId() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(Data.TABLE_NAME,
                new String[] {Data.COLUMN_ID,Data.COLUMN_FOTO},
                null, null, null, null, null);

        // cek query sukses dikesekusi
        Data data = null;
        if (cursor != null) {

            // cek apakah data kosong
            if (!dataKosong()) {
                try {
                    cursor.moveToLast();
                    data = new Data(
                            Integer.parseInt(cursor.getString(0)),
                            cursor.getString(1)
                    );
                } catch (Exception e) {
                    e.printStackTrace();

                }
            } else {
                data = new Data(0,"inisialisiasi");
            }
        }

        cursor.close();
        db.close();
        return data;
    }

    // hapus data
    public void hapusData(Data data) {
        SQLiteDatabase db = this.getWritableDatabase();

        try {
            db.delete(Data.TABLE_NAME, Data.COLUMN_ID+"=?", new String[]{String.valueOf(data.getId())});
        } catch (Exception e) {
            e.printStackTrace();
        }

        db.close();
    }
}
