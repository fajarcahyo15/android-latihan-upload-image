package io.github.fajarcahyo15.upload_multiple_files.view;

import android.content.Context;
import android.graphics.Bitmap;

import io.github.fajarcahyo15.upload_multiple_files.adapter.GambarAdapter;

/**
 * Created by root on 4/22/18.
 */

public interface MainView extends View {

    void onSetActionBar();

    void setRecyclerView(GambarAdapter adapter);

    void dislpayLoading();

    void destroyLoading();

}
